public class Task1 {

    public static char[] selectionSorting(char[] array) {
        int indexOfLeast;
        char temp;

        for (int i = 0; i < array.length - 1; i++) {
            indexOfLeast = i;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[indexOfLeast]) {
                    indexOfLeast = j;
                }
            }

            temp = array[indexOfLeast];
            array[indexOfLeast] = array[i];
            array[i] = temp;
        }

        return array;
    }

    public static char[] bubbleSorting(char[] array) {
        boolean isSorted = false;
        char temp;
        char charToCompare;

        while (!isSorted) {
            isSorted = true;

            for (int i = 0; i < array.length - 1; i++) {
                temp = array[i];
                charToCompare = array[i + 1];

                if (temp > charToCompare) {
                    isSorted = false;
                    array[i] = charToCompare;
                    array[i + 1] = temp;
                }
            }
        }

        return array;
    }

    public static char[] insertionSorting(char[] array) {

        for (int i = 1; i < array.length; i++) {
            char currentElement = array[i];
            int previousIndex = i - 1;
            while (previousIndex >= 0 && array[previousIndex] > currentElement) {
                array[previousIndex + 1] = array[previousIndex];
                array[previousIndex] = currentElement;
                previousIndex--;
            }
        }

        return array;
    }

}
