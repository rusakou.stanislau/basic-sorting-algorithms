public class Task2 {

    public static String[] selectionSorting(String[] array) {

        for (int i = 0; i < array.length - 1; i++) {
            int indexOfLeast = i;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j].compareTo( array[indexOfLeast] ) < 0) {
                    indexOfLeast = j;
                }
            }

            String temp = array[i];
            array[i] = array[indexOfLeast];
            array[indexOfLeast] = temp;
        }

        return array;
    }

    public static String[] bubbleSorting(String[] array) {
        boolean isSorted = false;
        String temp;
        String stringToCompare;

        while (!isSorted) {
            isSorted = true;

            for (int i = 0; i < array.length - 1; i++) {
                temp = array[i];
                stringToCompare = array[i + 1];

                if (temp.compareTo( stringToCompare ) > 0) {
                    isSorted = false;
                    array[i] = stringToCompare;
                    array[i + 1] = temp;
                }
            }
        }

        return array;
    }

    public static String[] insertionSorting(String[] array) {

        for (int i = 1; i < array.length; i++) {
            String currentElement = array[i];
            int previousIndex = i - 1;

            while (previousIndex >= 0 && array[previousIndex].compareTo( currentElement ) > 0) {
                array[previousIndex + 1] = array[previousIndex];
                array[previousIndex] = currentElement;
                previousIndex--;
            }
        }

        return array;
    }
}
