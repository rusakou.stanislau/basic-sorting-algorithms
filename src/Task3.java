import java.util.Arrays;

public class Task3 {

    public static void main(String[] args) {
        String[] names = new String[]{"Egor","Artiom","Egor","Vlad","Igor","Ivan","Kirill","Egor"};
        String[] surnames = new String[]{"Anufriev","Abikenov","Kalinchuk","Ilyin","Loginov","Minin","Fomichev","Baranec"};
        sortByNames(names, surnames);
    }

    public static void sortByNames(String[] names, String[] surnames) {
        boolean isSortedByName = false;
        String tempName;
        String nameToCompare;

        String tempSurname;
        String surnameToCompare;

        while (!isSortedByName) {
            isSortedByName = true;

            for (int i = 0; i < names.length - 1; i++) {
                tempName = names[i];
                nameToCompare = names[i + 1];

                tempSurname = surnames[i];
                surnameToCompare = surnames[i + 1];

                Boolean isCurrentNameBigger = tempName.compareTo( nameToCompare ) > 0;
                Boolean areNamesEqual = tempName.compareTo( nameToCompare ) == 0;
                Boolean isCurrentSurnameBigger = tempSurname.compareTo( surnameToCompare ) > 0;
                if (isCurrentNameBigger || (areNamesEqual && isCurrentSurnameBigger)) {
                    isSortedByName = false;
                    names[i] = nameToCompare;
                    names[i + 1] = tempName;

                    surnames[i] = surnameToCompare;
                    surnames[i + 1] = tempSurname;
                }
            }
        }

        printArrayOfStrings(names);
        printArrayOfStrings(surnames);
    }

    public static void printArrayOfStrings(String[] array) {
        System.out.println(Arrays.toString(array));
    }

}
